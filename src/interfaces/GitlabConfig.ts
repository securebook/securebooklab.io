export interface GitlabConfig {
	apiUri: string;
	oAuthUri: string;
	oAuthTokenUri: string;
	oAuthClientId: string;
	oAuthRedirectId: string;
}