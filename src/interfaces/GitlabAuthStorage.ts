export interface GitlabAuthStorage {
	getVerifier(): string | null;
	setVerifier(verifier: string);
	getToken(): string | null;
	setToken(token: string);
	getRefreshToken(): string | null;
	setRefreshToken(token: string);
	clearToken();
}