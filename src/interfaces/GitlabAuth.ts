import { Auth } from "@interfaces/Auth";

export interface GitlabAuth extends Auth {
  processAuthCode(code: string): Promise<void>;
  refreshToken(): Promise<void>;
}