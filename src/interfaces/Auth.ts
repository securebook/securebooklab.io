export interface Auth {
	login();
	onLoginSucceeded(tokenData: { token: string, refresh_token: string });
	onLoginFailed(error: string, errorDescription: string);
	logout();
}