import { LocationManager } from "@interfaces/LocationManager";
import { GitlabConfig } from "@interfaces/GitlabConfig";
import { QueryBuilder } from "@interfaces/QueryBuilder";
import { GitlabAuthStorage } from "@interfaces/GitlabAuthStorage";
import { GitlabAuthData } from "@interfaces/GitlabAuthData";
import { GitlabAuth as IGitlabAuth } from "@interfaces/GitlabAuth";
import { AuthURLStorage } from "@interfaces/AuthURLStorage";
import { PassStorage } from "@interfaces/PassStorage";
import { getRandom } from "@utils/random";
import { sha256 } from "js-sha256";
import { PathManager } from "@interfaces/PathManager";

function generateCodeVerifier() {
	const alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~';
	let verifier = '';
	for (let i = 0; i < 128; i++) {
		verifier += alphabet[getRandom(0, alphabet.length - 1)];
	}
	return verifier;
}

function getCodeChallenge(verifier: string) {
  return btoa(String.fromCharCode(...new Uint8Array(sha256.digest(verifier))))
    .replace(/=/g, '')
		.replace(/\+/g, '-')
		.replace(/\//g, '_');
}

type Token = {
	status: 'success',
	token: string,
	refresh_token: string,
};

type TokenError = {
	status: 'error',
	error: string,
	error_description: string
};

type TokenOrError = Token | TokenError;

const MAX_AUTH_RETRIES = 3;
const MAX_REFRESH_RETRIES = 3;

export class GitlabAuth implements IGitlabAuth {
	private readonly locationManager: LocationManager;
	private readonly config: GitlabConfig;
	private readonly queryBuilder: QueryBuilder;
	private readonly authStorage: GitlabAuthStorage;
	private readonly authData: GitlabAuthData;
	private readonly authURLStorage: AuthURLStorage;
	private readonly passStorage: PassStorage;
	private readonly pathManager: PathManager;
	private authRetry: number;
	private refreshRetry: number;

	constructor(locationManager: LocationManager, pathManager: PathManager, config: GitlabConfig, queryBuilder: QueryBuilder, authStorage: GitlabAuthStorage, authData: GitlabAuthData, authURLStorage: AuthURLStorage, passStorage: PassStorage) {
		this.authRetry = 0;
		this.refreshRetry = 0;
		this.locationManager = locationManager;
		this.pathManager = pathManager;
		this.config = config;
		this.queryBuilder = queryBuilder;
		this.authStorage = authStorage;
		this.authData = authData;
		this.authURLStorage = authURLStorage;
		this.authData.data = this.getInitialStoredData();
		this.passStorage = passStorage;
	}

	private getInitialStoredData(): GitlabAuthData['data'] {
		const storedToken = this.authStorage.getToken();
		if (typeof storedToken === 'string') {
			return {
				status: 'Authenticated',
				token: storedToken
			};
		} else {
			return {
				status: 'Unauthenticated'
			}
		}
	}

	login() {
		this.authURLStorage.storeURL();
		const verifier = generateCodeVerifier();
		this.authStorage.setVerifier(verifier);
		this.locationManager.redirect(
			`${this.config.oAuthUri}?${this.queryBuilder.getStringFromQuery({
				client_id: this.config.oAuthClientId,
				redirect_uri: this.config.oAuthRedirectId,
				response_type: 'code',
				scope: 'api',
				code_challenge: getCodeChallenge(verifier),
				code_challenge_method: 'S256',
			})}`
		);
	}

	async refreshToken() {
		for (;;) {
			const tokenResult = await this.getRefreshedTokenOrError();
			if (tokenResult.status === 'success') {
				this.onLoginSucceeded(tokenResult);
			} else {
				if (this.refreshRetry < MAX_REFRESH_RETRIES) {
					this.refreshRetry++;
					continue;
				} else {
					this.onLoginFailed(
						tokenResult.error,
						tokenResult.error_description
					);
				}
			}
			return;
		}
	}

	async processAuthCode(code: string) {
		for (;;) {
			const tokenResult = await this.getTokenOrError(code);
			if (tokenResult.status === 'success') {
				this.onLoginSucceeded(tokenResult);
			} else {
				if (this.authRetry < MAX_AUTH_RETRIES) {
					this.authRetry++;
					continue;
				} else {
					this.onLoginFailed(
						tokenResult.error,
						tokenResult.error_description
					);
				}
			}
			this.pathManager.onAuthCompleted();
			return;
		}
	}

	private async getRefreshedTokenOrError(): Promise<TokenOrError> {
		const refreshToken = this.authStorage.getRefreshToken();
		if (!refreshToken) {
			return {
				status: 'error',
				error: `Refresh token was not saved correctly`,
				error_description: '',
			}
		}
		const codeVerifier = this.authStorage.getVerifier();
		if (!codeVerifier) {
			return {
				status: 'error',
				error: `Code verifier was not saved correctly`,
				error_description: '',
			}
		}
		let response: Response;
		try {
			response = await fetch(this.config.oAuthTokenUri, {
				method:  'post',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
				},
				body: this.queryBuilder.getStringFromQuery({
					client_id: this.config.oAuthClientId,
					refresh_token: refreshToken,
					grant_type: 'refresh_token',
					redirect_uri: this.config.oAuthRedirectId,
					code_verifier: codeVerifier
				})
			});
		} catch (e) {
			return {
				status: 'error',
				error: `Could not refresh token`,
				error_description: (e as Error).toString(),
			}
		}
		let responseJson: any;
		try {
			responseJson = await response.json();
		} catch (e) {
			return {
				status: 'error',
				error: 'Could not parse auth token response as json',
				error_description: (e as Error).toString(),
			}
		}
		if (!responseJson.access_token) {
			return {
				status: 'error',
				error: 'Auth token response contains no auth token',
				error_description: `Found no auth token in ${JSON.stringify(responseJson, null, 2)}`,
			}
		}
		return {
			status: 'success',
			token: responseJson.access_token,
			refresh_token: responseJson.refresh_token,
		}
	}

	private async getTokenOrError(code: string): Promise<TokenOrError> {
		const codeVerifier = this.authStorage.getVerifier();
		if (!codeVerifier) {
			return {
				status: 'error',
				error: `Code verifier was not saved correctly`,
				error_description: '',
			}
		}
		let response: Response;
		try {
			response = await fetch(this.config.oAuthTokenUri, {
				method:  'post',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
				},
				body: this.queryBuilder.getStringFromQuery({
					client_id: this.config.oAuthClientId,
					code,
					grant_type: 'authorization_code',
					redirect_uri: this.config.oAuthRedirectId,
					code_verifier: codeVerifier
				})
			});
		} catch (e) {
			return {
				status: 'error',
				error: `Could not fetch token`,
				error_description: (e as Error).toString(),
			}
		}
		let responseJson: any;
		try {
			responseJson = await response.json();
		} catch (e) {
			return {
				status: 'error',
				error: 'Could not parse auth token response as json',
				error_description: (e as Error).toString(),
			}
		}
		if (!responseJson.access_token) {
			return {
				status: 'error',
				error: 'Auth token response contains no auth token',
				error_description: `Found no auth token in ${JSON.stringify(responseJson, null, 2)}`,
			}
		}
		return {
			status: 'success',
			token: responseJson.access_token,
			refresh_token: responseJson.refresh_token,
		}
	}

	onLoginSucceeded({ token, refresh_token }: Token) {
		this.authData.data = {
			status: 'Authenticated',
			token
		};
		this.authStorage.setToken(token);
		this.authStorage.setRefreshToken(refresh_token);
	}

	onLoginFailed(error: string, errorDescription: string) {
		this.authData.data = {
			status: 'Error',
			error,
			errorDescription
		};
		this.authStorage.clearToken();
	}

	logout() {
		this.authData.data = {
			status: 'Unauthenticated'
		};
		this.authStorage.clearToken();
		this.passStorage.delete();
	}
}