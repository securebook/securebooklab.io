import { GitlabAuthStorage as IGitlabAuthStorage } from "@interfaces/GitlabAuthStorage";

export class GitlabAuthStorage implements IGitlabAuthStorage {
	getVerifier() {
		return window.localStorage.getItem('verifier');
	}

	setVerifier(verifier: string) {
		window.localStorage.setItem('verifier', verifier);
	}

	getToken() {
		return window.localStorage.getItem('token');
	}

	setToken(token: string) {
		window.localStorage.setItem('token', token);
	}

	getRefreshToken() {
		return window.localStorage.getItem('refresh_token');
	}

	setRefreshToken(token: string) {
		window.localStorage.setItem('refresh_token', token);
	}

	clearToken() {
		window.localStorage.removeItem('token');
	}
}