import { GitlabConfig } from "@interfaces/GitlabConfig";

export const gitlabConfig: GitlabConfig = (process.env.NODE_ENV !== 'development')
	? GITLAB_CONFIG
	: {
		apiUri: "https://gitlab.com/api/v4",
		oAuthUri: "https://gitlab.com/oauth/authorize",
		oAuthTokenUri: "https://gitlab.com/oauth/token",
		oAuthClientId: "7fc2d48d8e06b0f051da1ec0344f1bebb0403e35a7e5cb8a1a7fb7758711e3c1",
		oAuthRedirectId: "http://127.0.0.1:8080/?token_response=gitlab_oauth",
	}
;