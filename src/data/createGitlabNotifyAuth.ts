import { Location } from "@interfaces/Location";
import { GitlabAuth } from "@interfaces/GitlabAuth";

export function createGitlabNotifyAuth(location: Location, auth: GitlabAuth) {
	return {
		notifyAuthWhenLoggedIn() {
			const query = location.query;
			if (query.token_response === 'gitlab_oauth') {
				auth.processAuthCode(query.code);
			}
		}
	}
}